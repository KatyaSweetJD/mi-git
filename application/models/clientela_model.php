<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class clientela_model extends CI_Model
{
	
	public function vista()
	{
		$vis = $this->db->get('cliente');
		return $vis->result();
	}

	public function bill()
	{
		$this->db->select('f.id_factura, cl.nombre, f.fecha, mp.nombre');
		$this->db->from('factura f');
		$this->db->join('cliente cl','cl.id_cliente = f.id_cliente');
		$this->db->join('modo_pago mp','mp.num_pago = f.num_pago');
		$query = $this->db->get();
		return $query->result();
	}
}

?>