<footer class="page-footer font-small" style="background-color: #1A4C8B;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 py-5">
        <div class="mb-5 flex-center" align="center">
          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg mr-md-5 mr-3 fa-2x" style="color: snow;"> </i>
          </a>
          <a class="tw-ic">
            <i class="fab fa-twitter fa-lg mr-md-5 mr-3 fa-2x" style="color: snow;"> </i>
          </a>
          <a class="dev-ic">
            <i class="fab fa-deviantart fa-lg mr-md-5 mr-3 fa-2x" style="color: snow;"> </i>
          </a>
          <a class="li-ic">
            <i class="fab fa-linkedin-in fa-lg mr-md-5 mr-3 fa-2x" style="color: snow;"> </i>
          </a>
          <a class="ins-ic">
            <i class="fab fa-instagram fa-lg mr-md-5 mr-3 fa-2x" style="color: snow;"> </i>
          </a>
          <a class="you-ic">
            <i class="fab fa-youtube fa-lg fa-2x" style="color: snow;"> </i>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-copyright text-center py-3" style="color: snow;">© 2019 Copyright KatySapphire</div>
</footer>
</html>