<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class start extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('start_model');
	}

	public function index()
	{
		$data = array(
			'page_title' => 'Revisión',
			'vista' => 'home',
			'data_view' => array());
		$this->load->view('/Template/Main',$data);
	}

	public function look()
	{
		$this->start_model->list();
		$this->load->view('/Template/Main');
	}
}

?>