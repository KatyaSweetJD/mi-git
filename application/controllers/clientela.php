<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class clientela extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('clientela_model');
	}

	public function index()
	{
		$data = array(
			'page_title' => 'Clientes',
			'vista' => 'cliente',
			'data_view' => array());
		$cli = $this->clientela_model->vista();
		$data['vi'] = $cli;
		$this->load->view('/Template/Main',$data);
	}

	public function bill_modal()
	{
		$cli = $this->clientela_model->bill_mod();
		$data['bi'] = $cli;
		$this->load->view('/Template/Main',$data);
	}
}

?>