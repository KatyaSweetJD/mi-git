<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class producto_con extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pro_mod');
	}

	public function index()
	{
		$data = array(
			'page_title' => 'Productos',
			'vista' => 'pro',
			'data_view' => array());
		$pro = $this->pro_mod->vel();
		$data['ve'] = $pro;
		$this->load->view('/Template/Main', $data);
	}
}

?>